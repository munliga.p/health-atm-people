import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isAuthenticated: false,
    user: null,
    user_data: null
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login(state, action) {
            state.isAuthenticated = true;
            state.user = action.payload;
        },
        user_data(state, action) {
            state.isAuthenticated = true;
            state.user_data = action.payload;
        },
        logout(state) {
            state.isAuthenticated = false;
            state.user = null;
        }
    },
});

export const { login, user_data, logout } = authSlice.actions;
export default authSlice.reducer;